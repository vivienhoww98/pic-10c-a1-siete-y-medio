#include "cards.h"
#include <cstdlib>
#include <iostream>
#include <iomanip>

/* 
You might or might not need these two extra libraries 
#include <iomanip>
#include <algorithm>
*/


/* *************************************************
   Card class
   ************************************************* */

/* 
   Default constructor for the Card class.
   It could give repeated cards. This is OK.
   Most variations of Blackjack are played with 
   several decks of cards at the same time.
*/
Card::Card(){
   int r = 1 + rand() % 4;
   switch (r) {
      case 1: suit = OROS; break;
      case 2: suit = COPAS; break; 
      case 3: suit = ESPADAS; break;
      case 4: suit = BASTOS; break; 
      default: break;
   }

   r = 1 + rand() % 10;  
   switch (r) {
      case  1: rank = AS; break; 
      case  2: rank = DOS; break; 
      case  3: rank = TRES; break; 
      case  4: rank = CUATRO; break; 
      case  5: rank = CINCO; break; 
      case  6: rank = SEIS; break; 
      case  7: rank = SIETE; break; 
      case  8: rank = SOTA; break; 
      case  9: rank = CABALLO; break; 
      case 10: rank = REY; break; 
      default: break;
   }
}

// Accessor: returns a string with the suit of the card in Spanish 
string Card::get_spanish_suit() const { 
   string suitName;
   switch (suit) {
      case OROS: suitName = "oros"; break;
      case COPAS: suitName = "copas"; break; 
      case ESPADAS: suitName = "espadas"; break;
      case BASTOS: suitName = "bastos"; break; 
      default: break;
   }
   return suitName;
}

// Accessor: returns a string with the rank of the card in Spanish 
string Card::get_spanish_rank() const { 
   string rankName;
   switch (rank) {
      case AS: rankName = "As"; break; 
      case DOS: rankName = "Dos"; break; 
      case TRES: rankName = "Tres"; break; 
      case CUATRO: rankName = "Cuatro"; break; 
      case CINCO: rankName = "Cinco"; break; 
      case SEIS: rankName = "Seis"; break; 
      case SIETE: rankName = "Siete"; break; 
      case SOTA: rankName = "Sota"; break; 
      case CABALLO: rankName = "Caballo"; break; 
      case REY: rankName = "Rey"; break; 
      default: break;
   }
   return rankName;
}



// Accessor: returns a string with the suit of the card in English 
// This is just a stub! Modify it to your liking.
string Card::get_english_suit() const {
	std::string engSuitName;
	switch (suit) {
	case OROS: engSuitName = "coins"; break;
	case COPAS: engSuitName = "cups"; break;
	case ESPADAS: engSuitName = "swords"; break;
	case BASTOS: engSuitName = "clubs"; break;
	default: break;
	}
	return engSuitName;
}

// Accessor: returns a string with the rank of the card in English 
// This is just a stub! Modify it to your liking.
string Card::get_english_rank() const { 
	string engRankName;
	switch (rank) {
	case AS: engRankName = "One"; break;
	case DOS: engRankName = "Two"; break;
	case TRES: engRankName = "Three"; break;
	case CUATRO: engRankName = "Four"; break;
	case CINCO: engRankName = "Five"; break;
	case SEIS: engRankName = "Six"; break;
	case SIETE: engRankName = "Seven"; break;
	case SOTA: engRankName = "Jack"; break;
	case CABALLO: engRankName = "Knight"; break;
	case REY: engRankName = "King"; break;
	default: break;
	}
	return engRankName;
}



// Assigns a numerical value to card based on rank.
// AS=1, DOS=2, ..., SIETE=7, SOTA=10, CABALLO=11, REY=12
int Card::get_rank() const {
   return static_cast<int>(rank) + 1 ;
}

// Comparison operator for cards
// Returns TRUE if card1 < card2
bool Card::operator < (Card card2) const {
   return rank < card2.rank;
}



/* *************************************************
   Hand class
   ************************************************* */
// Implemente the member functions of the Hand class here.

// Hand class default constructor
// Creates an empty hand with no cards
// Cards are added via add_cards
Hand::Hand() : total(0) {}

// adds card to hand
void Hand::add_card(const Card& c){

	//add card to vector
	cards.push_back(c);
	// add to point total
	int c_rank = c.get_rank();
	if (c_rank <= 7)
		total += c_rank; // number cards (value = rank)
	else{
		total += 0.5; //face cards (rank = 0.5)
	}
}
// return point total
double Hand::get_total() const{
	return total;
}

void Hand::print_card(const Card& c) const{
	std::string spanish_card = c.get_spanish_rank() + " de " + c.get_spanish_suit();
	std::string english_card = "(" + c.get_english_rank() + " of " + c.get_english_suit() + ")";
	std::cout << std::setw(8) << "" << std::setw(20) << std::left << spanish_card << english_card << std::endl;
}
void Hand::print_one_card() const{
	Card target_card = cards.back();
	print_card(target_card);
}
void Hand::print_whole_hand() const{
	for (size_t i = 0, n = cards.size(); i < n; ++i)
		print_card(cards[i]);
}

void Hand::reset_hand(){
	cards = std::vector<Card>();
	total = 0;
}

/* *************************************************
   Player class
   ************************************************* */
// Implemente the member functions of the Player class here.

// constructor
// player starts with m amount of money
Player::Player(int m) : money(m) {}

int Player::get_money() const{
	return money;
}

void Player::gain_money(int amount){
	money += amount;
}
void Player::lose_money(int amount){
	money -= amount;
}

double Player::get_hand_total() const{
	return hand.get_total();
}

bool Player::bust() const{
	if (get_hand_total() > 7.5)
		return true;
	else
		return false;
}

void Player::print_recent() const{
	std::cout << "New card: " << std::endl;
	hand.print_one_card();
	std::cout << std::endl;
}
void Player::print_hand() const{
	hand.print_whole_hand();
}

void Player::draw_card(){
	Card n = Card();
	hand.add_card(n);
}

void Player::new_hand(){
	hand.reset_hand();
}


