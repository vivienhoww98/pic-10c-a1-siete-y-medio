#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <ctime>
#include <cstdlib>
#include "cards.h"
using namespace std;

// Global constants (if any)


// Non member functions declarations (if any)


// Non member functions implementations (if any)

int main(){

	std::srand(time(nullptr));

	Player user(100); // User starts with $100
	Player dealer(900); // Dealer starts with $900

	// start game with one card each
	user.draw_card();
	dealer.draw_card();

	int bet = 0;

	bool play_game = true;

	while (play_game == true) {		

		bool user_turn = true;
		bool dealer_turn = true;

		bool getbet = true;
		while (getbet == true){
			std::cout << "You have $" << user.get_money() << ". " << "Enter bet: ";
			std::cin >> bet;
			if (bet > user.get_money()){
				std::cout << "You have entered an invalid bet! " << std::endl;
				getbet = true;
			}
			else
				getbet = false;
		}

		while (user_turn == true) {	

			bool ask = true;

			std::cout << "Your cards: " << std::endl;
			user.print_hand();
			std::cout << "Your total is " << user.get_hand_total() << ". ";

			// Check if bust after calculating total
			if (user.bust()){
				user_turn = false;
				dealer_turn = false;
				ask = false;
			}

			while (ask == true){
				// Ask if user wants another card, collects input
				std::cout << "Do you want another card (y/n)? ";
				char yorn;
				cin >> yorn;

				// What happens depends on user's input
				if (yorn == 'y'){
					// User wants another card, draw card
					user_turn = true;
					user.draw_card();
					user.print_recent();
					ask = false;
				}
				else if (yorn == 'n'){
					// User doesn't want another card, user's turn ends
					user_turn = false;
					ask = false;
				}
				else{
					// Invalid input, ask again
					std::cout << "Invalid input! \nPlease try again. ";
					ask = true;
				}	
			}
		}

		// If player busted, don't even bother with dealer
		if (dealer_turn == false)
			std::cout << "You busted!" << std::endl;

		while (dealer_turn == true){	

			std::cout << "Dealer's cards: " << std::endl;
			dealer.print_hand();
			std::cout << "The dealer's total is " << dealer.get_hand_total() << ". \n\n";

			// Dealer stops drawing cards at 5.5
			if (dealer.get_hand_total() > 5.5)
				dealer_turn = false;
			else{
				dealer_turn = true;
				dealer.draw_card();
				dealer.print_recent();
			}
		}

		// Calculate score

		if (user.bust()){
			std::cout << "\nToo bad. You lose " << bet << "\n\n";
			user.lose_money(bet);
			dealer.gain_money(bet);
		}
		else if (dealer.bust()){
			std::cout << "You win " << bet << "\n\n";
			user.gain_money(bet);
			if (dealer.get_money() < bet)
				dealer.lose_money(dealer.get_money());
			dealer.lose_money(bet);
		}
		else{ // player < 7.5 && dealer < 7.5
			if (user.get_hand_total() > dealer.get_hand_total()){
				std::cout << "You win " << bet << "\n\n";
				user.gain_money(bet);
				if (dealer.get_money() < bet)
					dealer.lose_money(dealer.get_money());
				dealer.lose_money(bet);
			}
			else if (dealer.get_hand_total() > user.get_hand_total()){
				std::cout << "\nToo bad. You lose " << bet << "\n\n";
				user.lose_money(bet);
				dealer.gain_money(bet);
			}
			else // player == dealer
				std::cout << "Nobody wins!" << std::endl;
		}

		// Game ends if user has $0 or dealer has lost $900
		// Dealer losing $900 = User winning $900
		// For user to win need $900 + initial $100 = $1000
		if (user.get_money() == 0){
			std::cout << "\nYou have $0. GAME OVER! \n";
			std::cout << "Come back when you have more money.\n\n";
			std::cout << "Bye!";
			play_game = false;
		}
		else if (dealer.get_money() == 0){
			std::cout << "\n\nCongratulations, you beat the casino!\n\n";
			std::cout << "Bye!";
			play_game = false;
		}
		else{ // Start another round
			user.new_hand();
			user.draw_card();
			dealer.new_hand();
			dealer.draw_card();
			play_game = true;
		}
	}

	

	// keep a log of every round in gamelog.txt

	string block;
	std::cout << "Block in play";
	std::cin >> block;
	

   return 0;
}